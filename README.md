### youlan-app 优蓝电商app

#### 运行
1. yarn/cnpm install/npm install
2. yarn dev

#### 发布
1. yarn build

#### 架构
```
|—— youlan-app
   |—— index.html
   |—— public
      |—— favicon.ico
   |—— src
      |—— main.ts
      |—— pages
         |—— index.vue
      .
      .
      .
```
