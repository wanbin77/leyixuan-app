import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite';
import { VantResolver } from '@vant/auto-import-resolver';
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
      vue(),
      Components({
      resolvers: [VantResolver()],
    }),
  ],
  /** 配置代理服务器 -- server */
  server: {
    /** 配置端口 */
    host: '10.7.163.72',
    port: 9000,
    /** 配置代理规则 -- 仅开发使用 */
    proxy: {
      /** http://localhost:9000/apis/show/list */
      /** 哪些地址执行一下代理规则 -- 你的请求地址里边有/apis(那么不能使用/api /ap /a)，那么执行以下代理规则 */
      "/apis": {
        /** 代理的目标服务器 -- 地址变为 http://www.yszhsc.com:15666/apis/show/list */
        target: 'http://www.yszhsc.com:15666',
        changeOrigin: true,
        /** 当代理服务器请求目标服务器的时候，需要把代理关键字/apis替换掉 */
        /** http://www.yszhsc.com:15666/show/list? */
        rewrite: function(pathname) {
          return pathname.replace('/apis', '')
        }
      }
    }
  }
})
