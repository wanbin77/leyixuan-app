import { createApp } from 'vue'
import App from './pages/index.vue'
import router from './router/index'
import { pinia } from './store'

// 2. 引入组件样式
import 'vant/lib/index.css';

/** 引入全局样式 Preprocessor dependency "less" not found. Did you install it? Try `npm install -D less`. -- https://ant.design/docs/spec/colors-cn */
import './style/index.less'
import myDynamics from './compuents'

createApp(App)
    .use(router)
  .use(pinia)
  .use(myDynamics)
    .mount('#app')
