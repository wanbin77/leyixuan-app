import { NavigationGuardNext, RouteLocation } from "vue-router";
import { useCommonStore } from "../store";

export default function interceptor(to: RouteLocation, _: RouteLocation, next: NavigationGuardNext) {
    if (to.meta.nologin) {
        next()
    } else {
        let token = useCommonStore().token;
        if (token) {
            next()
        } else {
            next('/')
            console.log(12222);
            
        }
    }
}