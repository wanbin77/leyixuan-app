import Login from '../pages/login/index.vue';
import Welcome from '../pages/login/welcome.vue';
const enroll = () => import('../pages/login/enroll.vue');
const Home = () => import('../pages/home/index.vue');
const cont = () => import('../pages/login/cont.vue');
const video = () => import('../pages/video/video.vue');

export default [
    {path: '/', component: Welcome, meta: {nologin: true}},
    { path: '/home', component: Home ,meta:{name:'mainTabs'} },
    { path: '/login', component: Login, meta: { nologin: true, name: "backTabs"} },
    { path: '/enroll', component: enroll, meta: { nologin: true, name: "backTabs" } },
    { path: '/video', component: video, meta: { nologin: true  ,name: "mainTabs"} },
    { path: '/cont', component: cont, meta: { nologin: true ,name: "backTabs" } },
    { path: '/cart', component: () => import('../pages/cart/cart.vue'),meta:{name:'mainTabs'}  },
    { path: '/type', component: () => import('../pages/type/type.vue'),meta:{name:'mainTabs'}  },
    { path: '/me', component: () => import('../pages/me/me.vue'),meta:{name:'mainTabs'} },
    {path: '/type/hot', component: () => import('../pages/type/hot.vue') ,meta:{name:'backTabs'}}
]