/**
 * @author bing.wang
 * @time 2023-10-18 23 PM
 * @description 定义路由和实现路由拦截
 */
import { createRouter, createWebHistory, Router } from 'vue-router';
import interceptor from './interceptor';
import routes from './routes';

const router: Router = createRouter({
    history: createWebHistory(),
    routes: routes
})

router.beforeEach(interceptor)

export default router;