/**
 * @description 定义状态数据管理器，实现缓存和持久化
 */
import { createPinia } from 'pinia'
import useCommonStore from './common';
import persisteStatePlugin from './persiste';

const pinia = createPinia();

pinia.use(persisteStatePlugin)

export {
    pinia,
    useCommonStore
}