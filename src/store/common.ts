/**
 * @description 定义通用的状态数据管理库
 */
import { defineStore } from "pinia"
import { initState } from "./persiste"
import { CommonStore } from "../vite-env"
import { ref } from "vue"
let _initState = initState()

const useCommonStore = defineStore(
    'youlan-common',
    {
        state(): CommonStore {
            return {
                hotList: ref({}),
                token: _initState.token || '',
                userInfo: _initState.userInfo || {}
            }
        }
    }
)
export default useCommonStore