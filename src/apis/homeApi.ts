import { GOODS_BANNER_URL } from "../config/urls.cfg";
import request from "./ajax";


// 获取轮播
export const getBanner = () => {
    return request({
        url: GOODS_BANNER_URL,
        method: 'GET'
    })
}   