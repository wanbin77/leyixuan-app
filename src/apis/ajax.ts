/**
 * @author deming.su
 */
import axios, { AxiosResponse, InternalAxiosRequestConfig } from "axios";
import { useCommonStore } from "../store";
import {YlRequest} from "../vite-env"


const Ajax = axios.create({
    baseURL: '/apis'
})

/** 请求拦截 */
// use(onFulfilled?: ((value: InternalAxiosRequestConfig) => InternalAxiosRequestConfig | Promise))
Ajax.interceptors.request.use(function(config: InternalAxiosRequestConfig) {
    /** 如果返回一个Promise，那么请求不会发送到后端 */
    // return Promise.reject({})
    /** 如果返回配置对象，那么会发送请求 -- 所以可以在这里添加数据 */
    config.headers.token = useCommonStore().token
    return config
})

/** 封装请求为一个完整的请求方法 -- 解决async + await的问题(只能接收resolve) */
export default function request(req: YlRequest): Promise<any> {
    return new Promise(resolve => {
        Ajax.request({
            method: req.method || 'get',
            url: req.url,
            data: req.data || {},
            params: req.params || {}
        }).then(function({data}: AxiosResponse) {
            resolve(data)
        }).catch(function(e: any) {
            resolve(e)
        })
    })
}