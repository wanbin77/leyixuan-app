import { BASE64_UPLOAD_URL, GOODS_CATEGORY_LISTS_URL, GOODS_CATEGORY_LIST_URL, GOODS_DATA_TYPE_URL, GOODS_VIDEO_LIST_URL, SHOW_LIST_URL, USER_AUTO_LOGIN_URL, USER_LOGIN_URL, USER_REGISTER_URL,USER_VDT_CODE_URL } from "../config/urls.cfg";
import request from "./ajax";

/** 获取漫展数据接口 */
export function getShowListApi(page: number, size: number): Promise<any> {
    return request({
        url: SHOW_LIST_URL,
        params: {page, size}
    })
}

// 文件上传
export function base64UploadApi(file: string, name: string): Promise<any> {
    return request({
        url: BASE64_UPLOAD_URL,
        data: {file, name},
        method: 'POST'
    })
}

// 注册接口
export function userRegisterApi(data: any): Promise<any> {
    return request({
        url: USER_REGISTER_URL,
        data,
        method: 'PUT'
    })
}

// 验证电话接口
export function getVdtCodeApi(phone: number ): Promise<any> {
    return request({
        url: USER_VDT_CODE_URL,
        data: {phone: phone + ''},
        method: 'POST'
    })
}

// 自动登录
export function autoLoginApi(): Promise<any> { 
    return request({
        url: USER_AUTO_LOGIN_URL,
        method: 'POST'
    })
}

// 电话号码登录
export function phoneLoginApi(phone:number,code:string): Promise<any> { 
    return request({
        url: USER_LOGIN_URL,
        data: {phone:phone + '',code},
        method: 'POST'
    })
}

// 获取首页商品数据
export function getHomeGoodsApi(page: number, size: number): Promise<any> { 
    return request({
        url: GOODS_DATA_TYPE_URL,
        params: {page, size}
    })
}

// 获取首页分类数据
export function getHomeCategoryApi(): Promise<any> { 
    return request({
        url:GOODS_CATEGORY_LIST_URL
    })
}

// 获取分类数据列表
export function getCategoryListApi(): Promise<any> { 
    return request({
        url:  GOODS_CATEGORY_LISTS_URL
    })
}

// 视频列表请求
export function getVideoListApi(): Promise<any> { 
    return request({
        url: GOODS_VIDEO_LIST_URL
    })
}

