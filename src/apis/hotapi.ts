import { AROUND_LIST, EXHIBITION_LIST, GAME_DEVICE, HOT_FIGURINES } from "../config/gethoturl"
import request from "./ajax"


// 获取手办
export const getHOTfig= () => {
    return request({
        url: HOT_FIGURINES,
        method: 'GET'
    })
} 

// 周边列表
export const getPeripheryList = () => { 
    return request({
        url: AROUND_LIST,
        method: 'GET'
    })
}

// 游戏设备
export const getGameDevice = () => { 
    return request({
        url: GAME_DEVICE,
        method: 'GET'
    })
}

// 漫展列表
export const getMuseumList = () => {   
    return request({
        url: EXHIBITION_LIST,
        method: 'GET'
    })  
}