/// <reference types="vite/client" />

/** 定义全局vue文件 */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

export interface YlRequest {
  url: string;
  params?: any;
  data?: any;
  method?: 'POST' | 'GET' | 'DELETE' | 'PUT';
}

export interface CommonStore{
  token: string;
  userInfo: any;
  hotList:any
}

export interface UserInfo {
  id: string;
  name: string;
  password: string;
  photo: string;
  sex: string;
  address?: string;
  birthday?: string;
  phone: string;
  email?: string;
  remark?: string;
}
/**
 * 图片缩放
 * @param {File} _file 需要缩放的file对象
 * @returns Promise<string> 返回的base64字节码
 */
function scaleImage(_file: File): Promise<string> {
    return new Promise(resolve => {
        let _fr: FileReader = new FileReader();
        /** 等fileReader读取完成以后得到一个回调方法 */
        _fr.onload = function() {
            /** 把base64转为一个Image对象 */
            let _img: HTMLImageElement = new Image()
            /** 等Image对象加载完成图片 */
            _img.onload = function() {
                /** 把我们的图片等比缩放后画到一个画布上 */
                let _canvas: HTMLCanvasElement = document.createElement('canvas')
                /** 得到画笔 */
                let _ctx: CanvasRenderingContext2D = _canvas.getContext('2d') as CanvasRenderingContext2D;
                /** 设置需要得到的图片大小 */
                let _width: number = 512;
                _canvas.width = _width;
                _canvas.height = _width;

                /** 算图片的缩放比率，然后得到高度和宽度 */
                let _destWidth: number;
                let _destHeight: number;
                if (_img.width < _width && _img.height < _width) {
                    _destHeight = _img.height;
                    _destWidth = _img.width;
                } else {
                    let _rate = (_img.width >= _img.height ? _img.width : _img.height) / _width;
                    /** 图片缩放后的宽度 */
                    _destWidth = _img.width / _rate;
                    _destHeight = _img.height / _rate;
                }

                /** 开始画图 */
                _ctx.drawImage(_img, 0, 0, _img.width, _img.height, (_width - _destWidth) / 2, (_width - _destHeight) / 2, _destWidth, _destHeight);
                /** 得到base64 */
                resolve(_canvas.toDataURL())
            }
            /** 开始加载图片 */
            _img.src = _fr.result as string;
        }
        /** 把一个文件读成一个base64字节码 */
        _fr.readAsDataURL(_file);
    })
}
/**
 * 校验是否为手机号
 * @param phone 
 * @returns 
 */
function validatePhone(phone: number): boolean {
    return /^1[3-9]{1}[0-9]{9}$/.test(phone + '')
}

export {
    encodeApi,
    decodeApi,
    scaleImage,
  validatePhone,
    
}