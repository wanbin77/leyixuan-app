/** 获取漫展的列表url */
export const SHOW_LIST_URL: string = '/show/list';

/** base64文件上传url */
export const BASE64_UPLOAD_URL: string = '/upload/file/base64'

/** 注册url地址 */
export const USER_REGISTER_URL: string = '/register'

/** 获取验证码url */
export const USER_VDT_CODE_URL: string = '/get/code'

// 电话验证码登录url
export const USER_LOGIN_URL: string = '/login/byphone'

// 自动登录url
export const USER_AUTO_LOGIN_URL: string = '/auto/login'

// 获取首页商品分类列表
export const GOODS_CATEGORY_LIST_URL: string = '/home/types'

// 获取商品数据
export const GOODS_DATA_TYPE_URL: string = '/home/recommend'

// 获取分类列表
export const GOODS_CATEGORY_LISTS_URL: string = '/main/types'

// 轮播数据图
export const GOODS_BANNER_URL: string = '/banner/list'

// 视频列表
export const GOODS_VIDEO_LIST_URL: string = '/videos/list'


