/** 定义pinia缓存key */
export const PINIA_CACHE_KEY = 'PINIA_cache_key'
/** 定义记住手机号的常量 */
export const REMEMBER_CACHE_KEY = 'REMEMBER_CACHE_KEY'
/** 定义是否免登陆的常量 */
export const AUTO_CACHE_KEY = 'auto_cache_key'
/** 定义加解密key */
export const CRYPTO_KEY = 'U2FsdG&kX19f*^tJmgue94Hs4%#@jpaLW+O2Z4GKXN4='