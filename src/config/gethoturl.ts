// 手办雕像
export const HOT_FIGURINES = '/hot/kits'

// 周边列表
export const AROUND_LIST = '/hot/around'

// 游戏设备
export const GAME_DEVICE = '/hot/device'

// 漫展列表
export const EXHIBITION_LIST = '/hot/show'