import { App } from "vue";
import mytabs from './mytabs.vue'
import maintabs from './maintab.vue'
import backtab from './blacktab.vue'


// vue插件导出一个app对象，需要的地方引入使用就行
export default function myDynamics(App: App) { 
    // 注册全局组件
        App.component('myTabs', mytabs),
        App.component('mainTabs', maintabs),
        App.component('backTabs', backtab)
}